public class Board2{
	private static final int FINISH=3;
	private Die die1;
	private Die die2;
	//private boolean[] closedTiles;
	private int[][] tileDies;
	public Board2(){
		this.die1=new Die();
		this.die2=new Die();
		this.tileDies=new int[6][6];
	}
	public String toString(){
		String s="";
		for(int i=0;i<tileDies.length;i++){
			for(int x=0;x<tileDies[i].length;x++){
				s=s+tileDies[i][x];
			}
			s=s+"   ";
		}
		return s;
	}
	public boolean playATurn(){
		this.die1.roll();
		this.die2.roll();
		System.out.println("Die 1: "+this.die1.toString()+"\n"+"Die 2: "+this.die2.toString());
		System.out.println();
		if(tileDies[die1.getPips()-1][die2.getPips()-1]<FINISH){
			tileDies[die1.getPips()-1][die2.getPips()-1]=tileDies[die1.getPips()-1][die2.getPips()-1]+1;
			if(tileDies[die1.getPips()-1][die2.getPips()-1]==FINISH){
				return true;
			}
			else{
				return false;
			}
		}
		throw new IllegalArgumentException("The code should not reach here and this is here to overcome the compiler error for missing the return statemet.");
	}
}